# A Linux kernel module implementing the MORUS authenticated encryption algorithm family
**IMPORTANT:** This repository contains code that is no longer maintained and contains bugs. The code has been merged upstream into the Linux kernel and is available since version 4.18. Note that the kernel has to be built with the corresponding config options set to `m`/`y` for the new algorithms to be included in the resulting kernel build. Your Linux distribution may or may not enable these options when building their kernel.

This project aims to implement the MORUS AEAD algorithms as specified in [the 3rd round CAESAR submission](https://competitions.cr.yp.to/round3/morusv2.pdf) as a crypto module for the Linux kernel.

The project implements both MORUS-640 and MORUS-1280. It includes optimized implementations for the x86-64 architecture. MORUS-640 is implemented using SSE2 and MORUS-1280 both using AVX2 and also using SSE2 only.

## Building

To build the MORUS modules, you need to have the header files for the Linux kernel installed (`sudo apt-get install linux-headers-generic` on Ubuntu). Then, just run `make`.

## Installing

**WARNING**: The modules are still in development and may crash or break your machine! It is highly recommended that you only use them inside a virtual machine.

To install the modules, run `./load-all.sh` (as root). To remove the modules, run `./unload.sh`.

To see if the tests passed, run `dmesg | less +G`.
