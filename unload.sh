#!/bin/bash

rmmod morus_test.ko 2>/dev/null
rmmod morus1280-avx2.ko 2>/dev/null
rmmod morus1280-sse2.ko 2>/dev/null
rmmod morus1280-glue.ko 2>/dev/null
rmmod morus1280.ko 2>/dev/null
rmmod morus640-sse2.ko 2>/dev/null
rmmod morus640-glue.ko 2>/dev/null
rmmod morus640.ko 2>/dev/null
rmmod blockwalk.ko 2>/dev/null
