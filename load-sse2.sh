#!/bin/bash

modprobe cryptd

insmod morus640.ko || exit 1
insmod morus640-glue.ko || exit 1
insmod morus640-sse2.ko
insmod morus1280.ko || exit 1
insmod morus1280-glue.ko || exit 1
insmod morus1280-sse2.ko
insmod morus_test.ko || exit 1
