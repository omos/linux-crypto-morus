TEMPLATE = lib
CONFIG -= qt
CONFIG += staticlib

DEFINES += __KERNEL__

ARCH=x86
SRC_PROJECT_PATH = $$PWD
LINUX_VERSION = $$system(uname -r)
LINUX_HEADERS_PATH = /lib/modules/$$LINUX_VERSION/build

INCLUDEPATH += $$LINUX_HEADERS_PATH/include
INCLUDEPATH += $$LINUX_HEADERS_PATH/arch/$$ARCH/include

buildmod.commands = make -C $$LINUX_HEADERS_PATH M=$$SRC_PROJECT_PATH modules
cleanmod.commands = make -C $$LINUX_HEADERS_PATH M=$$SRC_PROJECT_PATH clean
QMAKE_EXTRA_TARGETS += buildmod cleanmod

HEADERS += \
    morus-common.h \
    morus640-glue.h \
    morus1280-glue.h \
    morus_tv.h

SOURCES += \
    morus640.c \
    morus1280.c \
    morus_test.c \
    morus640-glue.c \
    morus640-sse2-asm.S \
    morus640-sse2-glue.c \
    morus1280-glue.c \
    morus1280-sse2-asm.S \
    morus1280-sse2-glue.c \
    morus1280-avx2-asm.S \
    morus1280-avx2-glue.c

DISTFILES += \
    Makefile
