#include <linux/crypto.h>
#include <linux/scatterlist.h>
#include <linux/completion.h>
#include <linux/module.h>

#include <crypto/internal/aead.h>

#include "morus_tv.h"

struct result {
	struct completion comp;
	int err;
};

static void result_complete(struct crypto_async_request *req, int err)
{
	struct result *res = req->data;

	if (err == -EINPROGRESS)
		return;

	res->err = err;
	complete(&res->comp);
}

static int aead_do_sync(int (*func)(struct aead_request *req),
			struct aead_request *req)
{
	struct result res;
	int err;

	res.err = 0;
	init_completion(&res.comp);

	aead_request_set_callback(req, CRYPTO_TFM_REQ_MAY_BACKLOG,
				  &result_complete, &res);
	err = func(req);
	switch (err) {
	case 0:
		break;
	case -EINPROGRESS:
	case -EBUSY:
		wait_for_completion(&res.comp);
		err = res.err;
		if (err == 0)
			break;
		/* fall through */
	default:
		break;
	}
	return err;
}

static int run_test_case(const char *algname,
			 const struct crypto_morus_test_case *vec,
			 unsigned int number)
{
	int err = 0;
	int failed = 0;
	struct crypto_aead *cipher = NULL;
	struct aead_request *req = NULL;
	u8 *buffer = NULL, *buffer_iv, *buffer_ad, *buffer_crypt, *buffer_tag;
	struct scatterlist sg[1];
	unsigned int buffer_len;

	cipher = crypto_alloc_aead(algname, 0, 0);
	if (IS_ERR(cipher)) {
		printk("morus_test: ERROR allocating cipher!\n");
		err = PTR_ERR(cipher);
		cipher = NULL;
		goto out;
	}

	buffer_len = crypto_aead_ivsize(cipher) + vec->assoc_data_len +
			vec->plaintext_len + vec->tag_len;
	buffer = kmalloc(buffer_len, GFP_KERNEL);
	if (!buffer) {
		printk("morus_test: ERROR allocating buffer!\n");
		goto out;
	}
	buffer_iv = buffer;
	buffer_ad = buffer_iv + crypto_aead_ivsize(cipher);
	buffer_crypt = buffer_ad + vec->assoc_data_len;
	buffer_tag = buffer_crypt + vec->plaintext_len;

	sg_init_one(sg, buffer_ad, buffer_len - crypto_aead_ivsize(cipher));

	err = crypto_aead_setauthsize(cipher, vec->tag_len);
	if (err) {
		printk("morus_test: ERROR setting authsize!\n");
		goto out;
	}

	err = crypto_aead_setkey(cipher, vec->key, vec->key_len);
	if (err) {
		printk("morus_test: ERROR setting key!\n");
		goto out;
	}

	req = aead_request_alloc(cipher, GFP_KERNEL);
	if (IS_ERR(req)) {
		printk("morus_test: ERROR allocating request!\n");
		err = PTR_ERR(req);
		req = NULL;
		goto out;
	}

	memcpy(buffer_iv, vec->nonce, crypto_aead_ivsize(cipher));
	memcpy(buffer_ad, vec->assoc_data, vec->assoc_data_len);
	memcpy(buffer_crypt, vec->plaintext, vec->plaintext_len);

	aead_request_set_tfm(req, cipher);
	aead_request_set_ad(req, vec->assoc_data_len);
	aead_request_set_crypt(req, sg, sg, vec->plaintext_len, buffer_iv);

	err = aead_do_sync(crypto_aead_encrypt, req);
	if (err) {
		printk("morus_test: ERROR encrypting!\n");
		goto out;
	}

	if (crypto_memneq(buffer_crypt, vec->ciphertext, vec->plaintext_len)) {
		failed += 1;
		printk("morus_test: %u-encryption-ciphertext: Testcase failed!\n", number);
	}

	if (crypto_memneq(buffer_tag, vec->tag, vec->tag_len)) {
		failed += 1;
		printk("morus_test: %u-encryption-tag: Testcase failed!\n", number);
	}

	memcpy(buffer_iv, vec->nonce, crypto_aead_ivsize(cipher));
	memcpy(buffer_ad, vec->assoc_data, vec->assoc_data_len);
	memcpy(buffer_crypt, vec->ciphertext, vec->plaintext_len);
	memcpy(buffer_tag, vec->tag, vec->tag_len);

	aead_request_set_ad(req, vec->assoc_data_len);
	aead_request_set_crypt(req, sg, sg, vec->plaintext_len + vec->tag_len,
			       buffer_iv);

	err = aead_do_sync(crypto_aead_decrypt, req);
	if (err == -EBADMSG) {
		err = 0;
		failed += 1;
		printk("morus_test: %u-decryption-tag: Testcase failed!\n", number);
	} else if (err) {
		printk("morus_test: ERROR decrypting!\n");
		goto out;
	}

	if (crypto_memneq(buffer_crypt, vec->plaintext, vec->plaintext_len)) {
		failed += 1;
		printk("morus_test: %u-decryption-plaintext: Testcase failed!\n", number);
	}

out:
	if (buffer)
		kfree(buffer);
	if (cipher)
		crypto_free_aead(cipher);
	if (req)
		aead_request_free(req);
	return err < 0 ? err : failed;
}

static int crypto_morus_run_tests(void)
{
	unsigned int i = 0;
	int res, failed = 0;

	printk("morus_test: Running tests for morus640...\n");
	for (i = 0; i < ARRAY_SIZE(crypto_morus640_test_cases); i++) {
		printk("morus_test: Running testcase %u...\n", i);
		res = run_test_case("morus640", &crypto_morus640_test_cases[i], i);
		if (res < 0) {
			return -EINVAL;
		}
		failed += res;
	}

	printk("morus_test: Running tests for morus1280...\n");
	for (i = 0; i < ARRAY_SIZE(crypto_morus1280_test_cases); i++) {
		printk("morus_test: Running testcase %u...\n", i);
		res = run_test_case("morus1280", &crypto_morus1280_test_cases[i], i);
		if (res < 0) {
			return -EINVAL;
		}
		failed += res;
	}

	if (failed) {
		printk("morus_test: FAIL: %i tests failed!\n", failed);
	} else {
		printk("morus_test: OK!\n");
	}
	return 0;
}

static int __init crypto_morus_test_module_init(void)
{
	int err = crypto_morus_run_tests();
	if (err) {
		printk("morus_test: ERROR: %i\n", err);
	}
	return 0;
}

static void __exit crypto_morus_test_module_exit(void)
{
}

module_init(crypto_morus_test_module_init);
module_exit(crypto_morus_test_module_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ondrej Mosnacek <omosnacek@gmail.com>");
MODULE_DESCRIPTION("MORUS AEAD mode -- tests");
