obj-m += morus640.o morus1280.o morus_test.o
obj-m += morus640-glue.o morus1280-glue.o

ifeq ($(CONFIG_X86_64),y)
	morus640-sse2-objs += morus640-sse2-asm.o morus640-sse2-glue.o
	morus1280-sse2-objs += morus1280-sse2-asm.o morus1280-sse2-glue.o
	morus1280-avx2-objs += morus1280-avx2-asm.o morus1280-avx2-glue.o

	obj-m += morus640-sse2.o
	obj-m += morus1280-sse2.o
	obj-m += morus1280-avx2.o
endif

ifeq ($(KERNEL_BUILD),)
	ifeq ($(KERNEL_VERSION),)
		KERNEL_VERSION=$(shell uname -r)
	endif
	KERNEL_BUILD=/lib/modules/$(KERNEL_VERSION)/build
endif

all:
	make -C $(KERNEL_BUILD) M=$(PWD) modules

clean:
	make -C $(KERNEL_BUILD) M=$(PWD) clean
